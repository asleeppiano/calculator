import test from 'ava';
import Calculator from '../src/ts/calculator';


test('tokenizer should work',  t => {
  const calc = new Calculator()
  t.deepEqual(calc.tokenize('2 + 3 * 44 / 0.2'), ['2', '+', '3', '*', '44' , '/', '0.2']);
  t.deepEqual(calc.tokenize('(2 + 3)* 44 / 0.2'), ['(', '2', '+', '3', ')' , '*', '44', '/', '0.2']);
  t.deepEqual(calc.tokenize('-1+1'), ['-1', '+', '1']);
  t.deepEqual(calc.tokenize('sin(2) + 2'), ['sin', '(', '2', ')', '+', '2']);
  t.deepEqual(calc.tokenize('pow(2 2'), ['pow', '(', '2', '2']);
  t.deepEqual(calc.tokenize('cur(1usd rub)'), ['cur', '(', '1usd', 'rub', ')']);
  t.deepEqual(calc.tokenize('cur(1usd rub) + 1'), ['cur', '(', '1usd', 'rub', ')', '+', '1']);
})

test('tokenizer should work throw exception if there is unknown symbol',  t => {
  const calc = new Calculator();
  const error = t.throws(() => {
    calc.tokenize('pow(2, 2)');
  });
  t.is(error.message, 'unknown symbol: ,');
});


// INFIXTOPOSTFIX

test('infixToPostfix should work', t => {
  const calc = new Calculator()
  t.is(calc.infixToPostfix('(2 + 3)* 44 / 0.2'), '2 3 + 44 * 0.2 /');
  t.is(calc.infixToPostfix('(1 - 2) * 3 + (4 - 2 * 6)'), '1 2 - 3 * 4 2 6 * - +');
  t.is(calc.infixToPostfix('sin(2) + 2'), '2 sin 2 +');
  t.is(calc.infixToPostfix('sin(2 + 2)'), '2 2 + sin');
  t.is(calc.infixToPostfix('sin(cos(3))'), '3 cos sin');
  t.is(calc.infixToPostfix('cos(2^3 + 2 * abs(-500))'), '2 3 ^ 2 -500 abs * + cos');
  t.is(calc.infixToPostfix('5 * 90%'), '5 90 % *');
  t.is(calc.infixToPostfix('log(2 2)'), '2 2 log');
  t.is(calc.infixToPostfix('cur(1eur rub)'), '1eur rub cur');
  t.is(calc.infixToPostfix('cur(1eur rub) + 1'), '1eur rub cur 1 +');
  t.is(calc.infixToPostfix(')('), '(');
})

test('infixToPostfix should throw "expect expression" exception', t => {
  const calc = new Calculator();
  const ex = t.throws(() => {
    calc.infixToPostfix('sin(cos())');
  });
  t.is(ex.message, 'expect expression');

  const ex2 = t.throws(() => {
    calc.infixToPostfix('sin(2 +)');
  });
  t.is(ex2.message, 'expect expression');
})

test('infixToPostfix should throw "missing bracket after function name" exception', t => {
  const calc = new Calculator();
  const ex = t.throws(() => {
    calc.infixToPostfix('sin 2 + 2)');
  });
  t.is(ex.message, 'missing ( after function name');
})


// POSTFIXEVAL

test('postfixEval should work', t => {
  const calc = new Calculator();
  t.is(calc.postfixEval('2 3 + 44 * 0.2 /'), 1100);
  t.is(calc.postfixEval('1 2 - 3 * 4 2 6 * - +'), -11);
  t.is(calc.postfixEval('2 2 + sin'), -0.7568024953079282);
  t.is(calc.postfixEval('2 sin 2 +'),  2.909297426825682);
  t.is(calc.postfixEval('2 3 ^ 2 -500 abs * + cos'),  -0.8999062670030438);
  t.is(calc.postfixEval('5 90 % *'), 4.5);
})

test('postfixEval should throw "Division by zero" exception', t => {
  const calc = new Calculator()
  const ex = t.throws(() => {
    calc.postfixEval('2 0 /');
  });
  t.is(ex.message, 'Division by zero');
})
