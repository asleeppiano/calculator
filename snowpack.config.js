module.exports = {
  mount: {
    public: "/",
    src: "/_dist_",
  },
  plugins: [
    "./webpack.bundle.js",
    [
      "@snowpack/plugin-run-script",
      {
        cmd: "postcss src/css/style.css --dir public/css",
        watch: "postcss src/css/style.css --dir public/css --watch",
      },
    ],
  ],
  buildOptions: {
    baseUrl: "/",
    minify: true,
  },
};
