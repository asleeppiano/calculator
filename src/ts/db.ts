import { openDB } from "idb";
import type { DBSchema, IDBPDatabase } from "idb";

export interface CalculatorDB extends DBSchema {
  history: {
    value: {
      expression: string;
      result: number;
      id: number;
    };
    key: number;
  };
}

type DBSchemaKey = "history"

class Database {
  name: string;
  db: IDBPDatabase<CalculatorDB>;

  constructor(name: string) {
    this.name = name;
  }

  async open() {
    try {
      this.db = await openDB<CalculatorDB>(this.name, 1, {
        upgrade(db) {
          db.createObjectStore("history", {
            keyPath: "id",
            autoIncrement: true,
          });
        },
      });
    } catch (e) {
      throw new Error("IndexedDB error: " + e);
    }
  }
  async write(objectStoreName: DBSchemaKey, value: any, key?: number) {
    try {
      const tx = this.db.transaction(objectStoreName, "readwrite");
      const store = tx.objectStore(objectStoreName);
      await store.put(value, key);
      await tx.done;
    } catch (e) {
      throw new Error("IndexedDB write error: " + e);
    }
  }
  async read(objectStoreName: DBSchemaKey, key: number) {
    try {
      const tx = this.db.transaction(objectStoreName);
      const store = tx.objectStore(objectStoreName);
      const res = await store.get(key);
      await tx.done;
      return res;
    } catch (e) {
      throw new Error("IndexedDB read error: " + e);
    }
  }
  async getKeysAmount(objectStoreName: DBSchemaKey) {
    try {
      const tx = this.db.transaction(objectStoreName);
      const store = tx.objectStore(objectStoreName);
      const res = await store.getAllKeys()
      await tx.done
      return res.length
    } catch (e) {
      throw new Error("IndexedDB get keys amount" + e)
    }
  }
  close() {
    if (this.db) {
      this.db.close();
    }
  }
}

export default Database;
