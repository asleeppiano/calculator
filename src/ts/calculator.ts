import { currencyRates, convertRates } from "./currency";

export default class Calculator {
  private operatorStack: Array<string>;
  private output: Array<string>;
  private operandStack: Array<string>;

  private wordCharRegex = new RegExp(/\w/);
  private letterRegex = new RegExp(/[a-zA-Z]/);
  private whitespaceRegex = new RegExp(/\s/);
  private operandRegex = new RegExp(/^[-+]?([0-9]*[.])?[0-9]+$/);
  private operatorRegex = new RegExp(/^(\+|-|\*|\/|\^)$/);
  private functionRegex = new RegExp(/^(abs|sqrt|sin|cos|tan|cotan)$/);
  private function2Regex = new RegExp(/^(log|cur)$/);
  private parenthesesRegex = new RegExp(/(\(|\))/);
  private currencyRegex = new RegExp(/(eur|rub|usd|gbp|jpy)/);
  private currencyValueRegex = new RegExp(/^\d+(eur|rub|usd|gbp|jpy)$/);

  public tokenize(str: string): Array<string> {
    const tokens = [];
    let i = 0;
    while (i < str.length) {
      if (this.whitespaceRegex.test(str[i])) {
        i++;
        continue;
      } else if (str[i] === "-" && (i === 0 || str[i - 1] === "(")) {
        if (this.operandRegex.test(str[i + 1])) {
          i++;
          let num = "-";
          while (
            (this.operandRegex.test(str[i]) || str[i] === ".") &&
            i < str.length
          ) {
            num += str[i];
            i++;
          }
          tokens.push(num);
        } else if (!str[i + 1]) {
          i++;
        }
      } else if (this.letterRegex.test(str[i])) {
        let funcName = "";
        let t = 0;
        while (this.letterRegex.test(str[i]) && i < str.length) {
          funcName += str[i];
          i++;
          t++;
          if (t > 6) {
            throw new Error("function name is too long");
          }
        }
        if (
          this.currencyRegex.test(funcName) &&
          this.operandRegex.test(tokens[tokens.length - 1])
        ) {
          tokens[tokens.length - 1] += funcName;
          continue;
        }
        tokens.push(funcName);
      } else if (this.operandRegex.test(str[i]) || str[i] === ".") {
        let num = "";
        while (
          this.operandRegex.test(str[i]) ||
          (str[i] === "." && i < str.length)
        ) {
          num += str[i];
          i++;
        }
        tokens.push(num);
      } else if (
        this.operatorRegex.test(str[i]) ||
        this.parenthesesRegex.test(str[i])
      ) {
        tokens.push(str[i]);
        i++;
      } else if (str[i] === "%") {
        tokens.push(str[i]);
        i++;
      } else {
        throw new TypeError("unknown symbol: " + str[i]);
      }
    }

    return tokens;
  }

  public infixToPostfix(infix: string): string {
    this.operatorStack = [];
    this.output = [];

    const tokens = this.tokenize(infix);

    if (tokens.length < 2) {
      return undefined;
    }

    // let inFunction = false
    tokens.forEach((token, i, arr) => {
      if (
        this.currencyValueRegex.test(token) ||
        this.currencyRegex.test(token) ||
        token === "%" ||
        (this.wordCharRegex.test(token) &&
          !this.functionRegex.test(token) &&
          !this.function2Regex.test(token))
      ) {
        this.output.push(token);
      } else if (token === "(") {
        this.operatorStack.push(token);
      } else if (token === ")") {
        let last = this.operatorStack.pop();
        if (this.operatorRegex.test(arr[i - 1]) || arr[i - 1] === "(") {
          throw new SyntaxError("expect expression");
        }
        while (last !== "(" && this.operatorStack.length > 0) {
          this.output.push(last);
          last = this.operatorStack.pop();
        }
      } else if (this.operatorRegex.test(token)) {
        const last = this.operatorStack[this.operatorStack.length - 1];
        if (this.priority(last) >= this.priority(token)) {
          this.operatorStack.pop();
          this.output.push(last);
          this.operatorStack.push(token);
        } else {
          this.operatorStack.push(token);
        }
      } else if (
        this.functionRegex.test(token) ||
        this.function2Regex.test(token)
      ) {
        if (arr[i + 1] !== "(") {
          throw new SyntaxError("missing ( after function name");
        }
        const last = this.operatorStack[this.operatorStack.length - 1];
        if (this.priority(last) >= this.priority(token)) {
          this.operatorStack.pop();
          this.output.push(last);
          this.operatorStack.push(token);
        } else {
          this.operatorStack.push(token);
        }
        // inFunction = true
      }
    });

    while (this.operatorStack.length > 0) {
      const last = this.operatorStack.pop();
      this.output.push(last);
    }
    return this.output.join(" ");
  }

  public postfixEval(postfix: string): number {
    this.operandStack = [];

    const tokens = postfix.split(" ");

    for (const token of tokens) {
      if (
        this.operandRegex.test(token) ||
        this.currencyValueRegex.test(token) ||
        this.currencyRegex.test(token)
      ) {
        this.operandStack.push(token);
      } else if (this.operatorRegex.test(token)) {
        const b = this.operandStack.pop();
        const a = this.operandStack.pop();
        const res = this.calculate(token, parseFloat(a), parseFloat(b));
        this.operandStack.push(res.toString());
      } else if (this.functionRegex.test(token)) {
        const a = this.operandStack.pop();
        if (!this.operandRegex.test(a.toString())) {
          throw new Error("it is not a number");
        }
        const res = this.calculate(token, parseFloat(a));
        if (Number.isNaN(res)) {
          throw new Error("not a number");
        }
        this.operandStack.push(res.toString());
      } else if (this.function2Regex.test(token)) {
        const b = this.operandStack.pop();
        const a = this.operandStack.pop();
        let res: number;
        if (token === "cur") {
          res = this.convert(token, a, b);
        } else {
          res = this.calculate(token, parseFloat(a), parseFloat(b));
        }
        this.operandStack.push(res.toString());
      } else if (token === "%") {
        const a = this.operandStack.pop();
        const res = this.calculate(token, parseFloat(a));
        this.operandStack.push(res.toString());
      }
    }
    const res = parseFloat(this.operandStack.pop());
    return res;
  }

  public convert(op: string, a: string, b: string): number {
    if (op === "cur") {
      const from = { name: "", value: 0 };
      let tmp = '';
      for (let i = 0; i < a.length; i++) {
        if (this.operandRegex.test(a[i])) {
          tmp += a[i];
          continue;
        }
        from.name += a[i];
      }
      from.value = parseFloat(tmp);
      from.name = from.name.toUpperCase()
      b = b.toUpperCase()
      return convertRates(currencyRates, from, b);
    }
    throw new Error("cannot convert unknown function: " + op);
  }

  public calculate(
    op: string,
    a: number,
    b: number = undefined
  ): number | never {
    switch (op) {
      case "+":
        return a + b;
      case "-":
        return a - b;
      case "*":
        return a * b;
      case "/":
        if(b === 0) {
          throw new Error("Division by zero");
        }
        return a / b;
      case "%":
        return a / 100;
      case "abs":
        return a > 0 ? a : -a;
      case "sqrt":
        return Math.sqrt(a);
      case "log":
        return Math.log(b) / Math.log(a);
      case "^":
        return Math.pow(a, b);
      case "sin":
        return Math.sin(a);
      case "cos":
        return Math.cos(a);
      case "tan":
        return Math.tan(a);
      case "cotan":
        return 1 / Math.tan(a);
      default:
        throw new Error("Operation is not supported");
    }
  }

  public run(str: string): number {
    const postfix = this.infixToPostfix(str);
    if (!postfix) {
      return undefined;
    }
    const result = this.postfixEval(postfix) || 0;

    if (Number.isSafeInteger(result)) {
      return result;
    } else if (Number.isNaN(result)) {
      return 0;
    } else if (result % 1 !== 0) {
      return Number(result.toFixed(4));
    }
    return undefined;
  }

  private priority(token: string): number {
    if (!token) {
      return -1;
    }
    switch (token) {
      case "log":
      case "cur":
        return 5;
      case "abs":
      case "sqrt":
      case "sin":
      case "cos":
      case "tan":
      case "cotan":
        return 4;
      case "^":
      case "*":
      case "/":
        return 3;
      case "+":
      case "-":
        return 2;
      case "(":
        return 1;
      default:
        throw new Error("no such operator" + token);
    }
  }
}
