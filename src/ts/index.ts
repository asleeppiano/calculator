// import "../css/style.css";
import { initDisplay } from './display'
import { initKeyboard } from './keyboard'
import { initCurrencyStore } from './currency'
import { initHistory } from './history'
import { hasTouchScreen } from './utils'

async function main() {
  initDisplay()
  initKeyboard()
  await initHistory()
  await initCurrencyStore()
  if(window.innerWidth < 678 && hasTouchScreen()) {
    const input = document.querySelector('.input')
    input.removeAttribute('contenteditable')
  }
}

main()

