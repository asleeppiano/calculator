import Database from "./db";

const DB_NAME = "store";
const OBJECT_STORE_NAME = "history";
const INIT_HISTORY_CAPACITY = 100;

const historyList = document.querySelector(".history-list");
export const history = [];

/**
 * param {string} expression
 * param {string} answer
 * return {HTMLDivElement} historyItem
 *
 * historyItem should looks like this:
 *
 * <li role="option" tabindex="0" class="history-item">
 *  <div class="line">
 *    <span class="expression">{expression}</span>
 *  </div>
 *  <div class="line">
 *    <span class="answer">{answer}</span>
 *  </div>
 * </li>
 */
function createHistoryItem(
  expression: string,
  answer: string,
  index: string
): HTMLLIElement {
  const historyItem = document.createElement("li");
  historyItem.classList.add("history-item");
  historyItem.dataset.index = index;

  const line = document.createElement("div");
  line.classList.add("line");
  const expressionElement = document.createElement("span");
  expressionElement.classList.add("expression");
  expressionElement.textContent = expression;
  line.append(expressionElement);
  historyItem.append(line);

  const line2 = document.createElement("div");
  line2.classList.add("line");
  const answerElement = document.createElement("span");
  answerElement.classList.add("answer");
  answerElement.textContent = answer;
  line2.append(answerElement);
  historyItem.append(line2);

  return historyItem;
}

function getHistoryItem(e: Event) {
  const input = document.querySelector(".input");
  let resultSpan = document.querySelector(".result");
  const clickedItem = (e.target as HTMLDivElement | HTMLSpanElement).closest(
    ".history-item"
  );
  const historyItem =
    history[parseInt((clickedItem as HTMLDivElement).dataset.index) - 1];
  if (!resultSpan) {
    input.textContent = historyItem.expression;
    resultSpan = document.createElement("span");
    resultSpan.classList.add("result");
    resultSpan.textContent = historyItem.result;
    input.insertAdjacentElement("beforebegin", resultSpan);
  } else {
    input.textContent = historyItem.expression;
    resultSpan.textContent = historyItem.result;
  }
}

function placeCaretAtEnd(el: HTMLElement) {
  if (
    typeof window.getSelection != "undefined" &&
    typeof document.createRange != "undefined"
  ) {
    const range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    const sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }
}

function handleHistoryKeydown(e: KeyboardEvent) {
  const active = document.querySelector<HTMLLIElement>(
    '.history-list .history-item[tabindex="0"]'
  );
  switch (e.key) {
    case "ArrowDown": {
      e.preventDefault();
      active.tabIndex = -1;
      let next = active.nextElementSibling as HTMLLIElement;
      if (!next) {
        next = historyList.firstElementChild as HTMLLIElement;
      }
      next.tabIndex = 0;
      next.focus();
      break;
    }
    case "ArrowUp": {
      e.preventDefault();
      active.tabIndex = -1;
      let prev = active.previousElementSibling as HTMLLIElement;
      if (!prev) {
        prev = historyList.lastElementChild as HTMLLIElement;
      }
      prev.tabIndex = 0;
      prev.focus();
      break;
    }
    case "Enter": {
      e.preventDefault();
      active.click();
      const input = document.querySelector<HTMLSpanElement>(".input");
      setTimeout(() => input.focus(), 16);
      placeCaretAtEnd(input);
      break;
    }
  }
}

const db = new Database(DB_NAME);
export async function initHistory() {
  try {
    await db.open();
    const amount = await db.getKeysAmount(OBJECT_STORE_NAME);
    let length = INIT_HISTORY_CAPACITY;
    if (amount < INIT_HISTORY_CAPACITY) {
      length = amount;
    }
    if (amount > 0) {
      for (let i = 1; i <= length; i++) {
        const historyData = await db.read(OBJECT_STORE_NAME, i);
        history.push(historyData);
        const historyItem = createHistoryItem(
          historyData.expression,
          historyData.result.toString(),
          i.toString()
        );
        if (i === 1) {
          historyItem.tabIndex = 0;
        }
        historyList.append(historyItem);
      }
    } else {
      historyList.append(document.createTextNode("empty :("));
    }
    historyList.addEventListener("click", getHistoryItem);
    historyList.addEventListener("keydown", handleHistoryKeydown);
    return () => {
      historyList.removeEventListener("click", getHistoryItem);
      historyList.removeEventListener("keydown", handleHistoryKeydown);
    };
  } catch (e) {
    // TODO add warning component
    console.log("ex", e);
  }
}

export async function addToHistory(expression: string, result: string) {
  if(history.length === 0) {
    historyList.textContent = ''
  }
  const index = historyList.childNodes.length;
  const historyItem = {
    expression,
    result: parseInt(result),
  };
  db.write(OBJECT_STORE_NAME, historyItem);
  historyList.prepend(createHistoryItem(expression, result, index.toString()));
  history.push(historyItem);
}
