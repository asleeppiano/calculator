const CURRENCY_RATES_API = 'https://api.exchangeratesapi.io/latest'
const BASE_CURRENCY = 'EUR'

type CurrencyItem = {
  value: number,
  name: string
}

export interface Rate {
  [key: string]: number;
}

export interface CurrencyRates {
  base: string;
  date: string;
  rates: Rate
}

export let currencyRates:CurrencyRates = null;

export async function initCurrencyStore() {
  currencyRates = await getCurrencyRate()
}


async function fetchCurrencyRates(api: string) {
  try {
    const res = await fetch(api)
    const json = await res.json()
    if(res.ok) {
      localStorage.setItem('currency-updated', JSON.stringify(Date.now()))
      return json
    } else {
      throw new Error('json is not ok')
    }
  } catch (e) {
    throw new Error(e)
  }
}

export async function getCurrencyRate() {
  const currencyRates = JSON.parse(localStorage.getItem('currency'))
  const now = new Date()
  const days = 3
  now.setDate(now.getDate() + days)
  const daysThreshold = new Date(now.getTime())

  if(currencyRates && new Date(currencyRates.date).getTime() < daysThreshold.getTime() || !currencyRates) {
    const rates = await fetchCurrencyRates(CURRENCY_RATES_API)
    localStorage.setItem('currency', JSON.stringify(rates))
    return rates
  }
  return currencyRates
}

export function convertRates(currencyRates: CurrencyRates, from: CurrencyItem, to: string) {
  if(!currencyRates) {
    return 0
  }
  if(from.name === BASE_CURRENCY) {
    return from.value * currencyRates.rates[to]
  } else {
    const eur = from.value / currencyRates.rates[from.name]
    return eur * currencyRates.rates[to]
  }
}
