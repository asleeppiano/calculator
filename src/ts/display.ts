import { setAllButtonCoordinates } from "./keyboard";

const display = document.querySelector(".display");
const input = document.querySelector(".input");
const basicSwitch = document.querySelector("#basic-mode-btn");
const scientificSwitch = document.querySelector("#scientific-mode-btn");

enum MODE {
  BASIC = "basic",
  SCIENTIFIC = "scientific",
}

let currentMode = MODE.BASIC;

function handleDisplayClick({ target }) {
  if (target.tagName === "DIV" && target.classList.contains('display')) {
    (input as HTMLInputElement).focus();
    if(input.textContent.trim() === '0') {
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents(input);
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }
}

function makeActive(mode: MODE) {
  if (mode === MODE.BASIC) {
    basicSwitch.classList.add("active");
    scientificSwitch.classList.remove("active");
    const keyboardScientificModeList = document.querySelectorAll(
      ".keyboard .scientific-mode"
    );
    keyboardScientificModeList.forEach((keyboard) => {
      keyboard.classList.remove("visible");
    });
    const keyboardBasicModeList = document.querySelectorAll(
      ".keyboard .basic-mode"
    );
    keyboardBasicModeList.forEach((keyboard) => {
      keyboard.classList.remove("invisible");
    });
    setTimeout(() => setAllButtonCoordinates(), 0);
  } else if (mode === MODE.SCIENTIFIC) {
    basicSwitch.classList.remove("active");
    scientificSwitch.classList.add("active");
    const keyboardScientificModeList = document.querySelectorAll(
      ".keyboard .scientific-mode"
    );
    keyboardScientificModeList.forEach((keyboard) => {
      keyboard.classList.add("visible");
    });
    const keyboardBasicModeList = document.querySelectorAll(
      ".keyboard .basic-mode"
    );
    keyboardBasicModeList.forEach((keyboard) => {
      keyboard.classList.add("invisible");
    });
    setTimeout(() => setAllButtonCoordinates(), 0);
  }
}

function handleSwitchMode({ target }) {
  currentMode = target.dataset.mode;
  makeActive(currentMode);
}

function handleInputClick({target}) {
  if(target.textContent.trim() === '0') {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(input);
    selection.removeAllRanges();
    selection.addRange(range);
  }
}

export function initDisplay() {
  display.addEventListener("click", handleDisplayClick);
  basicSwitch.addEventListener("click", handleSwitchMode);
  scientificSwitch.addEventListener("click", handleSwitchMode);
  input.addEventListener('click', handleInputClick);

  return {
    destroy: () => {
      display.removeEventListener("click", handleDisplayClick);
      basicSwitch.removeEventListener("click", handleSwitchMode);
      scientificSwitch.removeEventListener("click", handleSwitchMode);
      input.removeEventListener('click', handleInputClick);
    },
  };
}
