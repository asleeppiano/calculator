import Calculator from "./calculator";
import debounce from "lodash-es/debounce";
import { addToHistory } from "./history";

const keyboard: HTMLDivElement = document.querySelector(".keyboard");
const input: HTMLSpanElement = document.querySelector(".input");

const calc = new Calculator();
const functionRegex = new RegExp(/^(abs|sqrt|sin|cos|log|tan|cotan)$/);

function isFunction(funcName: string) {
  return functionRegex.test(funcName);
}

function clearInput() {
  input.textContent = "0";
  const resultSpan = document.querySelector(".result");
  if (resultSpan) {
    resultSpan.classList.add("remove-animation");
    setTimeout(() => {
      resultSpan.remove();
    }, 1000);
  }
}

function submit() {
  const resultSpan = document.querySelector(".result");
  if (input.textContent === "" && !resultSpan) {
    return;
  }
  addToHistory(input.textContent, resultSpan.textContent);
  input.textContent = resultSpan.textContent;
  resultSpan.classList.add("remove-animation");
  setTimeout(() => {
    resultSpan.remove();
  }, 1000);
  placeCaretAtEnd(input);
}

function update() {
  const resultSpan = document.querySelector(".result");
  let result: number;
  try {
    result = calc.run(input.textContent);
  } catch (e) {
    // TODO implement warning
    console.log("ex", e);
    return;
  }
  if (resultSpan && result !== undefined) {
    resultSpan.textContent = result.toString();
  } else if (result !== undefined) {
    const resultSpan = document.createElement("span");
    resultSpan.classList.add("result");
    resultSpan.textContent = result.toString();
    input.insertAdjacentElement("afterend", resultSpan);
  }
}

function placeCaretAtEnd(el: HTMLElement) {
  if (
    typeof window.getSelection != "undefined" &&
    typeof document.createRange != "undefined"
  ) {
    const range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    const sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }
}

function inputKeydown(e: KeyboardEvent) {
  switch (e.key) {
    case "Enter":
      e.preventDefault();
      submit();
      break;
    case "=":
      e.preventDefault();
      submit();
      break;
  }
}

function handleInput() {
  update();
}

function insertTextAtCursor(text: string) {
  const selection = window.getSelection();
  const range = selection.getRangeAt(0);
  range.deleteContents();
  const node = document.createTextNode(text);
  range.insertNode(node);
  range.collapse(false);
}

function handleButtonClick(e: Event) {
  if(input.textContent === '0') {
    input.textContent = ''
  }
  const buttonValue = (e.target as HTMLButtonElement).value;
  if (isFunction(buttonValue)) {
    const value = `${buttonValue}(`;
    if (document.activeElement !== input) {
      input.textContent += value;
    } else {
      insertTextAtCursor(value);
    }
  } else {
    input.textContent += buttonValue;
  }
  update();
}

// TODO move to index.ts
const keysDown = {};
/**
 * @param {KeyboardEvent} e
 * hotkeys:
 * ctrl+k - focus keyboard
 * ctrl+d - focus input
 * ctrl+h - focus history
 */
function handleDocumentKeydown(e: KeyboardEvent) {
  if (e.key === "Control") {
    keysDown[e.key] = true;
  }
  if (keysDown["Control"] && e.key === "k") {
    e.preventDefault();
    let buttonToFocus = document
      .querySelector<HTMLButtonElement>(
        '.keyboard button[tabindex="0"]'
      )
    if(!buttonToFocus) {
      buttonToFocus = document
        .querySelector<HTMLButtonElement>(
          '.keyboard button[data-pos-start="1,1"]'
        )
      buttonToFocus.tabIndex = 0
    }
    buttonToFocus.focus();
  }
  if (
    keysDown["Control"] &&
    e.key === "d" &&
    document.activeElement !== input
  ) {
    e.preventDefault();
    input.focus();
  }
  if (keysDown["Control"] && e.key === "h") {
    e.preventDefault();
    const historyList = document.querySelector<HTMLUListElement>(
      '.history-list .history-item[tabindex="0"]'
    );
    if (historyList) {
      historyList.focus();
    }
  }
}

function handleDocumentKeyup() {
  if (keysDown["Control"]) {
    keysDown["Control"] = false;
  }
}

function backspace() {
  input.textContent = input.textContent.substring(
    0,
    input.textContent.length - 1
  );
  if(input.textContent.length === 0) {
    input.textContent = '0'
  }
}

function handleKeyboardClick(e: Event) {
  const button = (e.target as HTMLElement).closest("button");
  switch (button.value) {
    case "Clear":
      clearInput();
      break;
    case "Backspace":
      backspace();
      break;
    case "=":
      submit();
      break;
    default:
      handleButtonClick(e);
  }
}

function findKey(x: number, y: number, direction: string) {
  let currKey = document.querySelector<HTMLButtonElement>(
    `.keyboard button[data-pos-start="${x},${y}"]`
  );
  if (!currKey) {
    currKey = document.querySelector<HTMLButtonElement>(
      `.keyboard button[data-pos-end="${x},${y}"]`
    );
  }
  switch (direction) {
    case "left": {
      x--;
      if (x < 1) {
        return null;
      }
      const keyEnd = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-end="${x},${y}"]`
      );
      if (keyEnd) {
        return keyEnd;
      }
      const [x1, y1] = currKey.dataset.posStart.split(",");
      if (x1 === "1") {
        return null;
      }
      const next = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-end="${parseInt(x1) - 1},${y1}"]`
      );
      return next;
    }
    case "right": {
      x++;
      if (x > 6) {
        return null;
      }
      const keyStart = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-start="${x},${y}"]`
      );
      if (keyStart) {
        return keyStart;
      }
      const [x1, y1] = currKey.dataset.posEnd.split(",");
      if (x1 === "6") {
        return null;
      }
      const next = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-start="${parseInt(x1) + 1},${y1}"]`
      );
      return next;
    }
    case "up": {
      y--;
      if (y < 1) {
        return null;
      }
      const keyEnd = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-end="${x},${y}"]`
      );
      if (keyEnd) {
        return keyEnd;
      }
      const [x1, y1] = currKey.dataset.posStart.split(",");
      if (y1 === "1") {
        return null;
      }
      let next = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-end="${x1},${parseInt(y1) - 1}"]`
      );
      if (!next) {
        next = document.querySelector<HTMLButtonElement>(
          `.keyboard button[data-pos-start="${x1},${parseInt(y1) - 1}"]`
        );
      }
      return next;
    }
    case "down": {
      y++;
      if (y > 5) {
        return null;
      }
      const keyStart = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-start="${x},${y}"]`
      );
      if (keyStart) {
        return keyStart;
      }
      const [x1, y1] = currKey.dataset.posEnd.split(",");
      if (y1 === "5") {
        return null;
      }
      let next = document.querySelector<HTMLButtonElement>(
        `.keyboard button[data-pos-start="${x1},${parseInt(y1) + 1}"]`
      );
      if (!next) {
        next = document.querySelector<HTMLButtonElement>(
          `.keyboard button[data-pos-end="${x1},${parseInt(y1) + 1}"]`
        );
      }
      return next;
    }
  }
}

function handleKeyboardKeydown(e: KeyboardEvent) {
  if (keyboard.contains(document.activeElement)) {
    const [x, y] = (e.target as HTMLButtonElement).dataset.posStart.split(",");
    switch (e.key) {
      case "ArrowUp": {
        e.preventDefault()
        if (y === "1") {
          return;
        }
        const nextElement = findKey(parseInt(x), parseInt(y), "up");
        if (!nextElement) {
          return;
        }
        (nextElement as HTMLButtonElement).focus();
        (e.target as HTMLButtonElement).tabIndex = -1
        nextElement.tabIndex = 0
        break;
      }
      case "ArrowDown": {
        e.preventDefault()
        if (y === "5") {
          return;
        }
        const nextElement = findKey(parseInt(x), parseInt(y), "down");
        if (!nextElement) {
          return;
        }
        (nextElement as HTMLButtonElement).focus();
        (e.target as HTMLButtonElement).tabIndex = -1
        nextElement.tabIndex = 0
        break;
      }
      case "ArrowRight": {
        e.preventDefault()
        if (x === "8") {
          return;
        }
        const nextElement = findKey(parseInt(x), parseInt(y), "right");
        if (!nextElement) {
          return;
        }
        (nextElement as HTMLButtonElement).focus();
        (e.target as HTMLButtonElement).tabIndex = -1
        nextElement.tabIndex = 0
        break;
      }
      case "ArrowLeft": {
        e.preventDefault()
        if (x === "1") {
          return;
        }
        const nextElement = findKey(parseInt(x), parseInt(y), "left");
        if (!nextElement) {
          return;
        }
        (nextElement as HTMLButtonElement).focus();
        (e.target as HTMLButtonElement).tabIndex = -1
        nextElement.tabIndex = 0
        break;
      }
    }
  }
}

const debouncedInput = debounce(handleInput, 150);

function cleanButtonCoordinates() {
  const buttons = document.querySelectorAll('.keyboard button')
  buttons.forEach(button => {
    button.removeAttribute('tabindex')
    button.removeAttribute('data-pos-start')
    button.removeAttribute('data-pos-end')
  })
}

// TODO write tests
export function setAllButtonCoordinates() {
  cleanButtonCoordinates()
  const buttonColumns = document.querySelectorAll<HTMLDivElement>(
    ".keyboard div"
  );
  let i = 1;
  buttonColumns.forEach((column) => {
    const columnStyles = window.getComputedStyle(column)
    if(columnStyles.getPropertyValue('display') === 'none') {
      return
    }
    let currentColumnAmount: number;
    if (column.classList.contains("one-col")) {
      currentColumnAmount = 1;
    } else if (column.classList.contains("two-col")) {
      currentColumnAmount = 2;
    } else if (column.classList.contains("three-col")) {
      currentColumnAmount = 3;
    } else {
      throw new Error("each column should be the size from one to three");
    }
    const buttons = column.querySelectorAll<HTMLButtonElement>("button");
    let j = 1;
    const iPrev = i;
    buttons.forEach((button) => {
      button.tabIndex = -1
      if (button.classList.contains("span-2")) {
        button.dataset.posStart = `${i},${j}`;
        i++;
        button.dataset.posEnd = `${i},${j}`;
      } else {
        button.dataset.posStart = `${i},${j}`;
        button.dataset.posEnd = `${i},${j}`;
      }

      if (i + 1 - iPrev === currentColumnAmount) {
        i = iPrev;
        j++;
        return;
      }
      i++;
    });
    i += currentColumnAmount;
  });
  const firstButton = document.querySelector<HTMLButtonElement>('.keyboard button[data-pos-start="1,1"]')
  firstButton.tabIndex = 0
}


function handleWindowResize() {
  setAllButtonCoordinates();
}

const debouncedWindowResize = debounce(handleWindowResize, 150)

export function initKeyboard() {
  document.addEventListener("keydown", handleDocumentKeydown);
  document.addEventListener("keyup", handleDocumentKeyup);
  keyboard.addEventListener("click", handleKeyboardClick);
  keyboard.addEventListener("keydown", handleKeyboardKeydown);
  input.addEventListener("keydown", inputKeydown);
  input.addEventListener("input", debouncedInput);
  window.addEventListener('resize', debouncedWindowResize)

  setAllButtonCoordinates();

  return {
    destroy: () => {
      document.removeEventListener("keydown", handleDocumentKeydown);
      document.removeEventListener("keyup", handleDocumentKeyup);
      keyboard.removeEventListener("click", handleKeyboardClick);
      keyboard.removeEventListener("keydown", handleKeyboardKeydown);
      input.removeEventListener("keydown", inputKeydown);
      input.removeEventListener("input", debouncedInput);
      window.removeEventListener('resize', debouncedWindowResize)
    },
  };
}
